extends Node2D

var group
var needed = 5
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	randomize()
	$Player.position = $StartPosition.position
	group = $Group.get_children().size()
	$Group.hide()
	$Player.hide()
	$Banners.hide()

func _process(delta):
	for i in $Group.get_children().size():
		if i < group:
			$Group.get_children()[i].show()
		else:
			$Group.get_children()[i].hide()
	if needed == 0:
		game_win()
	if group == 0:
		game_over()

func game_win():
	$Group.hide()
	$Player.hide()
	$Watchmen.hide()
	$Banners.show()
	$HUD.get_children()[4].hide()
	$HUD.get_children()[1].hide()
	$HUD.get_children()[3].show()
	$HUD.get_children()[0].show()
	
func game_over():
	$Group.hide()
	$Player.hide()
	$Watchmen.hide()
	$HUD.get_children()[4].hide()
	$HUD.get_children()[1].hide()
	$HUD.get_children()[2].show()
	$HUD.get_children()[0].show()

func _on_Player_hit():
	group -= 1
	$Player.position = $StartPosition.position

func _on_Door_entered():
	#group -= 1
	needed -= 1
	$HUD.get_children()[4].set_text(str("People needed: ", needed))
	$Player.position = $StartPosition.position

func _on_HUD_start_game():
	needed = 5
	group = $Group.get_children().size()
	$Player.position = $StartPosition.position
	$Group.show()
	$Watchmen.show()
	$Player.show()
	$Banners.hide()
	$HUD.get_children()[2].hide()
	$HUD.get_children()[3].hide()
	$HUD.get_children()[4].set_text(str("People needed: ", needed))
