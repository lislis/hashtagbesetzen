extends Area2D

signal alert

var screensize  # Size of the game window.
export (int) var speed = 3
export (int) var paddingLeft = 100
export (int) var paddingRight = 100
var dirs = ["toright", "toleft"]
var dir = ""
var moving = true

func _ready():
	screensize = get_viewport_rect().size
	dir = dirs[randi() % dirs.size()]
	$AnimatedSprite.animation =  "toright"

func _process(delta):
	if moving:
		if dir == "toleft":
			position.x += speed
			scale.x = -1
			if position.x >= screensize.x - paddingLeft:
				dir = "toright"
		else:
			position.x -= speed
			scale.x = 1
			if position.x <= 0 + paddingRight:
				dir = "toleft"
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()

func _on_Watchman_area_entered(area):
	moving = false
	$CollisionShape2D.disabled = true
	$Timer.start()
	
func _on_Timer_timeout():
	moving = true
	$CollisionShape2D.disabled = false
