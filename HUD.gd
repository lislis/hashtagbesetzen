extends CanvasLayer

signal start_game

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _on_Start_button_down():
	$Start.hide()
	$Score.show()
	$Sprite.show()
	emit_signal("start_game")
